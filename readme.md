
# nbserver

Provides a command-line interface for running a Jupyter notebook server inside a Docker container. Contains standard datascience packages (pandas, sklearn, tensorflow).

## Installation

Set the environment variable `NBSERVER_HOME` to the folder this project lives in

    export NBSERVER_HOME=/path/to/nbserver

and update your `PATH` to include the `bin` folder of `$NBSERVER_HOME`

    export PATH=$PATH:$NBSERVER_HOME/bin

It is recommend to save these commands in your `~/.bash_profile` file.

## Commands

To stand up a server, run 

    $ nbserver start

and navigate to http://localhost:8888. Any notebooks you save will be saved in a folder called `notebooks` in the `nbserver` project directory. 

To inspect logs, run

    $ nbserver logs

To stop the server, run

    $ nbserver stop

To inspect running containers, run

    $ nbserver status 

All commands support a `--repro` option that lets prints out what command is being run.
